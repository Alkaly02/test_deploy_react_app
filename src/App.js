import { Link, NavLink, Route, RouterProvider, Routes, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';
import './App.css';
import HomePage from './pages/HomePage';
import Posts from './pages/Posts';
import PostDetails from './pages/PostDetails';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route path='/' element={<HomePage />}>
        <Route path='posts' element={<Posts />}>
          <Route path=':id' element={<PostDetails />} />
        </Route>
      </Route>
    </Route>
  )
)


function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
