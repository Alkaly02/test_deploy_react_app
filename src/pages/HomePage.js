import React from 'react'
import { NavLink, Outlet } from 'react-router-dom'

const HomePage = () => {
    return (
        <div>
            <nav
                style={{
                    borderBottom: '1px solid',
                    display: 'flex',
                    justifyContent: 'space-between',
                    padding: '0 10px 0'
                }}>
                <h1>Lka dev blog</h1>
                <div>
                    <ul style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        listStyle: 'none',
                        padding: 0,
                        gap: 10
                    }}>
                        <li>
                            <NavLink to='/'>Accueil</NavLink>
                        </li>
                        <li>
                            <NavLink to='/posts'>Posts</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>

            <Outlet />
        </div>
    )
}

export default HomePage