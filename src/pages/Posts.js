import React, { useEffect, useState } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'

const Posts = () => {
    const [posts, setPosts] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos/')
            .then(response => response.json())
            .then(json => setPosts(json))
    }, [])

    return (
        <div style={{ maxWidth: '900px', margin: 'auto' }}>
            {
                posts && posts.map(post => {
                    return <div key={post.id} style={{ border: '1px solid gray', margin: '10px', display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: 20 }}>
                        <p>{post.title}</p>
                        <button onClick={() => {
                            console.log(post);
                            return navigate(post.id)
                        }}>Details</button>
                    </div>
                })
            }
            <Outlet />
        </div>
    )
}

export default Posts