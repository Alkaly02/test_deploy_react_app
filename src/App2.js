import { NavLink, Route, Routes } from 'react-router-dom';
import './App.css';
import { routes } from './routes/navigationRoutes';
import HomePage from './pages/HomePage';

const recursiveRoutes = (navigationRoutes) => {
    return navigationRoutes.map(route => {
        if (route.children) {
            <Route path={route.path} element={route.element}>
                {recursiveRoutes(route.children)}
            </Route>
        }
        return <Route key={route.path} path={route.path} element={route.element} />
    })
}

function App2() {
    return (
        <>
            <nav
                style={{
                    borderBottom: '1px solid',
                    display: 'flex',
                    justifyContent: 'space-between',
                    padding: '0 10px 0'
                }}>
                <h1>Lka dev blog</h1>
                <div>
                    <ul style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        listStyle: 'none',
                        padding: 0,
                        gap: 10
                    }}>
                        <li>
                            <NavLink to='/'>Accueil</NavLink>
                        </li>
                        <li>
                            <NavLink to='/about'>About</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
            <Routes>
                <Route path='/' element={<HomePage />}>
                    {
                        recursiveRoutes(routes)
                    }
                    {/* <Route path='' element={<Work2 />} />
                    <Route path='portfolio' element={<Porfolio2 />} /> */}
                </Route>
            </Routes>
        </>
    );
}

export default App2;
